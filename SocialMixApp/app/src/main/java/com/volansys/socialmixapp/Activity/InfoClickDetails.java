package com.volansys.socialmixapp.Activity;

import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.volansys.socialmixapp.R;
import com.volansys.socialmixapp.Utils.AppConstants;

public class InfoClickDetails extends BaseActivity {
    public TextView id, name, mobile,city,date,company,profile,gender;
    public  ImageView img;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_click_details);

        initComponent();
        prepareViews();
        setListeners();


    }

    @Override
    protected void initComponent() {
        id = (TextView) findViewById(R.id.idTv);
        name = (TextView) findViewById(R.id.name);
        mobile = (TextView) findViewById(R.id.mobile);
        city = (TextView) findViewById(R.id.city);
        img = (ImageView) findViewById(R.id.img);
        date = (TextView) findViewById(R.id.date);
        company = (TextView) findViewById(R.id.company);
        profile = (TextView) findViewById(R.id.profile);
        gender = (TextView) findViewById(R.id.gender);
    }

    @Override
    protected void prepareViews() {


        id.setText(String.valueOf(getIntent().getStringExtra(AppConstants.KEY_ID)));
        name.setText(String.valueOf(getIntent().getStringExtra(AppConstants.KEY_NAME_1)));
        mobile.setText(String.valueOf(getIntent().getStringExtra(AppConstants.KEY_MOBILE)));
        city.setText(String.valueOf(getIntent().getStringExtra(AppConstants.KEY_CITY)));
        if(String.valueOf(getIntent().getStringExtra(AppConstants.KEY_IMAGE)).equals(""))
        {
            Picasso.with(InfoClickDetails.this)
                    .load((Uri) null)
                    .resize(300, 300)
                    .into(img);
        }
        else
            Picasso.with(InfoClickDetails.this)
                    .load(String.valueOf(getIntent().getStringExtra(AppConstants.KEY_IMAGE)))
                    .resize(300, 300)
                    .into(img);

        company.setText(String.valueOf(getIntent().getStringExtra(AppConstants.KEY_COMPANY)));
        profile.setText(String.valueOf(getIntent().getStringExtra(AppConstants.KEY_PROFFESSIONAL)));
        gender.setText(String.valueOf(getIntent().getStringExtra(AppConstants.KEY_GENDER_1)));
    }

    @Override
    protected void setListeners() {

    }
}
