package com.volansys.socialmixapp.Fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.volansys.socialmixapp.R;
import com.volansys.socialmixapp.Utils.AppConstants;


public class PersondetailsFragment extends BaseFragment {

    View rootView;
    TextView user_name, user_email, user_gender, user_birthday;
    ImageView user_photo;
    private String name, birthday, gender, email, photo;

    public PersondetailsFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_persondetails, container, false);
        initComponent();
        prepareViews();
        setListeners();

        return rootView;
    }


    @Override
    protected void initComponent() {
        user_name = (TextView) rootView.findViewById(R.id.UserName);
        user_email = (TextView) rootView.findViewById(R.id.email);
        user_gender = (TextView) rootView.findViewById(R.id.gender);
        user_birthday = (TextView) rootView.findViewById(R.id.birth);
        user_photo = (ImageView) rootView.findViewById(R.id.profilePic);
    }

    @Override
    protected void prepareViews() {

        Bundle bundle = this.getArguments();
        name = bundle.getString(AppConstants.KEY_NAME);
        birthday = bundle.getString(AppConstants.KEY_BIRTH_DAY);
        gender = bundle.getString(AppConstants.KEY_GENDER);
        email = bundle.getString(AppConstants.KEY_EMAIL);
        photo = bundle.getString(AppConstants.KEY_PHOTO);
        Picasso.with(getActivity())
                .load(photo)
                .resize(300, 300)
                .into(user_photo);

        user_email.setText(email);
        user_name.setText(name);
        user_gender.setText(gender);
        user_birthday.setText(birthday);

    }

    @Override
    protected void setListeners() {

    }
}
