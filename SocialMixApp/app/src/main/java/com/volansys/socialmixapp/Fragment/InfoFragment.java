package com.volansys.socialmixapp.Fragment;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.volansys.socialmixapp.Activity.InfoClickDetails;
import com.volansys.socialmixapp.Adapter.PersonAdapter;
import com.volansys.socialmixapp.Model.HttpHandler;
import com.volansys.socialmixapp.Model.Person;
import com.volansys.socialmixapp.R;
import com.volansys.socialmixapp.Utils.AppConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import static com.facebook.FacebookSdk.getApplicationContext;


public class InfoFragment extends BaseFragment {

    ArrayList<HashMap<String, String>> contactList;
    ArrayList<Person> personList = new ArrayList<>();
    View rootView;
    private ProgressDialog pDialog;
    private RecyclerView recyclerView;
    private PersonAdapter personAdapter;

    public InfoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        rootView = inflater.inflate(R.layout.fragment_info, container, false);
        initComponent();
        prepareViews();
        setListeners();
        new GetContacts().execute();
        return rootView;
    }

    @Override
    protected void initComponent() {
        recyclerView = (RecyclerView) rootView.findViewById(R.id.personLv);
    }

    @Override
    protected void prepareViews() {
        contactList = new ArrayList<>();
    }

    @Override
    protected void setListeners() {
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));

        FloatingActionButton fab = (FloatingActionButton) rootView.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                boolean isSwitched = personAdapter.toggleItemViewType();
                recyclerView.setLayoutManager(isSwitched ? new LinearLayoutManager(getActivity()) : new GridLayoutManager(getActivity(), 2));
                personAdapter.notifyDataSetChanged();

            }
        });
    }


    public interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }

    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildAdapterPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildAdapterPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {
        }
    }


    private class GetContacts extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(false);
            pDialog.show();

        }

        @Override
        protected Void doInBackground(Void... arg0) {

            HttpHandler sh = new HttpHandler();

            String jsonStr = sh.makeServiceCall(AppConstants.URL_JSONDATA);

            if (jsonStr != null) {
                try {


                    JSONArray contacts = new JSONArray(jsonStr);

                    for (int i = 0; i < contacts.length(); i++) {
                        JSONObject c = contacts.getJSONObject(i);
                        Person person = new Person();
                        person.setId(c.getString(AppConstants.KEY_ID));
                        person.setName(c.getString(AppConstants.KEY_NAME_1));
                        person.setMobile(c.getString(AppConstants.KEY_MOBILE));
                        person.setCity(c.getString(AppConstants.KEY_CITY));
                        person.setImage(c.getString(AppConstants.KEY_IMAGE));
                        person.setDate(c.getString(AppConstants.KEY_DATE));
                        person.setCompany(c.getString(AppConstants.KEY_COMPANY));
                        person.setProfessional(c.getString(AppConstants.KEY_PROFFESSIONAL));
                        person.setGender(c.getString(AppConstants.KEY_GENDER_1));
                        personList.add(person);
                    }
                } catch (final JSONException e) {

                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (pDialog.isShowing()) {
                pDialog.dismiss();
                initView();
            }
        }

        private AlertDialog AskOption(final int position) {
            AlertDialog myQuittingDialogBox = new AlertDialog.Builder(getActivity())

                    .setTitle(AppConstants.KEY_DELETE)
                    .setMessage(AppConstants.KEY_DELETE_MESSAGE)

                    .setPositiveButton(AppConstants.KEY_DELETE, new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int whichButton) {
                            personAdapter.removeAt(position);
                            Toast.makeText(getActivity(), AppConstants.KEY_DELETE, Toast.LENGTH_LONG);
                            dialog.dismiss();
                        }

                    })


                    .setNegativeButton(AppConstants.KEY_CANCLE, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                            dialog.dismiss();

                        }
                    })
                    .create();
            return myQuittingDialogBox;

        }

        private void initView() {

            personAdapter = new PersonAdapter(personList);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setAdapter(personAdapter);
            recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new ClickListener() {
                @Override
                public void onClick(View view, int position) {

                    final Person person = personList.get(position);
                    Intent intent = new Intent(getActivity(), InfoClickDetails.class);

                    intent.putExtra(AppConstants.KEY_ID, person.getId());
                    intent.putExtra(AppConstants.KEY_NAME_1, person.getName());
                    intent.putExtra(AppConstants.KEY_MOBILE, person.getMobile());
                    intent.putExtra(AppConstants.KEY_CITY, person.getCity());
                    intent.putExtra(AppConstants.KEY_IMAGE, person.getImage());
                    intent.putExtra(AppConstants.KEY_DATE, person.getDate());
                    intent.putExtra(AppConstants.KEY_COMPANY, person.getCompany());
                    intent.putExtra(AppConstants.KEY_PROFFESSIONAL, person.getProfessional());
                    intent.putExtra(AppConstants.KEY_GENDER_1, person.getGender());
                    startActivity(intent);


                }

                @Override
                public void onLongClick(View view, int position) {

                    AlertDialog diaBox = AskOption(position);
                    diaBox.show();
                }
            }));
        }

    }


}

