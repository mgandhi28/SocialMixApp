package com.volansys.socialmixapp.Utils;

/**
 * Created by monil on 20/1/18.
 */

public class AppConstants {

    public static final String KEY_NAME="name";
    public static final String KEY_BIRTH_DAY="birthday";
    public static final String KEY_GENDER="gender";
    public static final String KEY_EMAIL="email";
    public static final String KEY_PHOTO="photo";
    public static final String URL_FACEBOOK="https://graph.facebook.com/";
    public static final String KEY_ID="id";
    public static final String KEY_NAME_1="Name";
    public static final String KEY_MOBILE="Mobile";
    public static final String KEY_CITY="City";
    public static final String KEY_DATE="Date";
    public static final String KEY_IMAGE="Image";
    public static final String KEY_COMPANY="Company";
    public static final String KEY_PROFFESSIONAL="Professional";
    public static final String KEY_GENDER_1="Gender";
    public static final String KEY_PICTURE="/picture";
    public static final String KEY_CONNECTED="Internet Connected";
    public static final String KEY_CONNECTION_LOST="Internet Connection Lost";
    public static final String KEY_MESSAGE="Please Grant Storage Permission from setting";
    public static final String KEY_ERROR="Login Error";
    public static final String KEY_DONE="Logout Successful";
    public static final String KEY_CONNECTION_FILTER="android.net.conn.CONNECTIVITY_CHANGE";
    public static final String URL_JSONDATA="http://iwebworld.info/jsontest/getData";
    public static final String KEY_DELETE="Delete";
    public static final String KEY_DELETE_MESSAGE="Do you want to Delete";
    public static final String KEY_CANCLE="Cancle";


}
