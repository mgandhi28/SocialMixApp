package com.volansys.socialmixapp.Activity;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.common.api.GoogleApiClient;
import com.volansys.socialmixapp.Fragment.InfoFragment;
import com.volansys.socialmixapp.Fragment.MusicFragment;
import com.volansys.socialmixapp.Fragment.PersondetailsFragment;
import com.volansys.socialmixapp.R;
import com.volansys.socialmixapp.Utils.AppConstants;

import io.fabric.sdk.android.Fabric;


public class MainActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener {

    private final String TAG = MainActivity.this.getClass().getName();
    DrawerLayout drawer;

    String name, birthday, gender, email, photo;
    private GoogleApiClient mGoogleApiClient;
    private BroadcastScreenReceiver mBroadcastScreenReceiver;
    private IntentFilter mIntentFilter;
    private Toolbar toolbar;
    private TextView internetTxt;

    public static void setSnackBar(View coordinatorLayout, String snackTitle) {
        Snackbar snackbar = Snackbar.make(coordinatorLayout, snackTitle, Snackbar.LENGTH_SHORT);
        snackbar.show();
        View view = snackbar.getView();
        TextView internetTxt = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
        internetTxt.setGravity(Gravity.CENTER_HORIZONTAL);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_main);

        initComponent();
        prepareViews();
        setListeners();

        name = getIntent().getStringExtra(AppConstants.KEY_NAME);
        birthday = getIntent().getStringExtra(AppConstants.KEY_BIRTH_DAY);
        gender = getIntent().getStringExtra(AppConstants.KEY_GENDER);
        email = getIntent().getStringExtra(AppConstants.KEY_EMAIL);
        photo = getIntent().getStringExtra(AppConstants.KEY_PHOTO);


        mGoogleApiClient = new GoogleApiClient.Builder(MainActivity.this)
                .addApi(Auth.GOOGLE_SIGN_IN_API)
                .build();
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        mBroadcastScreenReceiver = new BroadcastScreenReceiver();
        IntentFilter screenStateFilter = new IntentFilter(AppConstants.KEY_CONNECTION_FILTER);
        registerReceiver(mBroadcastScreenReceiver, screenStateFilter);

        final String[] permissions = new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE};
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.READ_EXTERNAL_STORAGE)) {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);

                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityCompat.requestPermissions(MainActivity.this, permissions, 0);
                    }
                });

                builder.show();
            } else {
                ActivityCompat.requestPermissions(this, permissions, 0);
            }
        }
        LoadFragment();

    }

    public void LoadFragment() {
        Fragment fragment = new PersondetailsFragment();
        Bundle bundle = new Bundle();
        bundle.putString(AppConstants.KEY_NAME, name);
        bundle.putString(AppConstants.KEY_GENDER, gender);
        bundle.putString(AppConstants.KEY_EMAIL, email);
        bundle.putString(AppConstants.KEY_BIRTH_DAY, birthday);
        bundle.putString(AppConstants.KEY_PHOTO, photo);
        fragment.setArguments(bundle);


        if (fragment != null) {

            android.app.FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.replace(R.id.frag_container, fragment);
            ft.commit();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // Unregister
        if (mBroadcastScreenReceiver != null) {
            unregisterReceiver(mBroadcastScreenReceiver);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        Fragment fragment = null;
        int id = item.getItemId();

        if (id == R.id.nav_info) {

            fragment = new InfoFragment();
        } else if (id == R.id.nav_music) {
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, AppConstants.KEY_MESSAGE, Toast.LENGTH_SHORT).show();
            } else {
                fragment = new MusicFragment();
            }


        } else if (id == R.id.nav_logout) {

            if (mGoogleApiClient.isConnected()) {

                mGoogleApiClient.connect();
                if (mGoogleApiClient.isConnected()) {
                    Auth.GoogleSignInApi.signOut(mGoogleApiClient);
                    mGoogleApiClient.disconnect();
                    mGoogleApiClient.connect();
                }

            }
            new GraphRequest(AccessToken.getCurrentAccessToken(), "/me/permissions/", null, HttpMethod.DELETE, new GraphRequest
                    .Callback() {
                @Override
                public void onCompleted(GraphResponse graphResponse) {

                    LoginManager.getInstance().logOut();

                }
            }).executeAsync();
            Toast.makeText(MainActivity.this, AppConstants.KEY_DONE, Toast.LENGTH_LONG);

            Intent intent = new Intent(MainActivity.this, LoginActivity.class);
            startActivity(intent);
            finish();


        } else if (id == R.id.nav_details) {
            fragment = new PersondetailsFragment();
            Bundle bundle = new Bundle();
            bundle.putString(AppConstants.KEY_NAME, name);
            bundle.putString(AppConstants.KEY_GENDER, gender);
            bundle.putString(AppConstants.KEY_EMAIL, email);
            bundle.putString(AppConstants.KEY_BIRTH_DAY, birthday);
            bundle.putString(AppConstants.KEY_PHOTO, photo);
            fragment.setArguments(bundle);

        }
        if (fragment != null) {

            android.app.FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.replace(R.id.frag_container, fragment);
            ft.commit();

        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void initComponent() {
         toolbar = (Toolbar) findViewById(R.id.toolbar);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
    }

    @Override
    protected void prepareViews() {

    }

    @Override
    protected void setListeners() {

    }


    public class BroadcastScreenReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent arg1) {

            boolean isConnected = arg1.getBooleanExtra(ConnectivityManager.EXTRA_NO_CONNECTIVITY, false);
            if (isConnected) {
                setSnackBar(drawer, AppConstants.KEY_CONNECTION_LOST);

            } else {
                setSnackBar(drawer, AppConstants.KEY_CONNECTED);

            }
        }
    }
}
