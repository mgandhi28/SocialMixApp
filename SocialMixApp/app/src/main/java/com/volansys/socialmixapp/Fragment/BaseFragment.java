package com.volansys.socialmixapp.Fragment;

import android.app.Fragment;

/**
 * Created by monil on 25/1/18.
 */

public abstract class BaseFragment extends Fragment {

    protected abstract void initComponent();
    protected abstract void prepareViews();
    protected abstract void setListeners();
}
