package com.volansys.socialmixapp.Activity;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.volansys.socialmixapp.R;
import com.volansys.socialmixapp.Utils.AppConstants;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

public class LoginActivity extends BaseActivity {

    private static final int RC_SIGN_IN = 9001;
    private final String TAG = LoginActivity.this.getClass().getName();
    CallbackManager callbackManager;
    TextView FacebookDataTextView;
    Button FBButton;
    LoginManager loginManager;
    String name, birthday, gender, email, FacebookUserID;
    AccessTokenTracker accessTokenTracker;
    private SignInButton signInButton;
    private GoogleApiClient mGoogleApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        callbackManager = CallbackManager.Factory.create();
        loginManager = com.facebook.login.LoginManager.getInstance();
        setContentView(R.layout.activity_login);


        initComponent();
        prepareViews();
        setListeners();


        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {


                GraphLoginRequest(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {

                Toast.makeText(LoginActivity.this, AppConstants.KEY_ERROR, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException exception) {

                Toast.makeText(LoginActivity.this, AppConstants.KEY_ERROR, Toast.LENGTH_SHORT).show();
            }

        });

        if (AccessToken.getCurrentAccessToken() != null) {

            GraphLoginRequest(AccessToken.getCurrentAccessToken());
        }

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();


    }

    // Method to access Facebook User Data.
    protected void GraphLoginRequest(AccessToken accessToken) {
        GraphRequest graphRequest = GraphRequest.newMeRequest(accessToken,
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject jsonObject, GraphResponse graphResponse) {
                        Intent i = new Intent(LoginActivity.this, MainActivity.class);
                        try {

                            // Adding all user info one by one into TextView.
                            name = jsonObject.getString(AppConstants.KEY_NAME);

                            birthday = jsonObject.getString(AppConstants.KEY_BIRTH_DAY);

                            gender = jsonObject.getString(AppConstants.KEY_GENDER);

                            email = jsonObject.getString(AppConstants.KEY_EMAIL);

                            FacebookUserID = AppConstants.URL_FACEBOOK + jsonObject.getString(AppConstants.KEY_ID) + AppConstants.KEY_PICTURE;


                            i.putExtra(AppConstants.KEY_NAME, name);
                            i.putExtra(AppConstants.KEY_BIRTH_DAY, birthday);
                            i.putExtra(AppConstants.KEY_GENDER, gender);
                            i.putExtra(AppConstants.KEY_EMAIL, email);
                            i.putExtra(AppConstants.KEY_PHOTO, FacebookUserID);
                            startActivity(i);
                            finish();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

        Bundle bundle = new Bundle();
        bundle.putString(
                "fields", "id,name,birthday,email,gender,picture");
        graphRequest.setParameters(bundle);
        graphRequest.executeAsync();


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(mGoogleApiClient);
        if (opr.isDone()) {

            GoogleSignInResult result = opr.get();
            handleSignInResult(result);
        } else {

            opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                @Override
                public void onResult(GoogleSignInResult googleSignInResult) {
                    handleSignInResult(googleSignInResult);
                }
            });
        }
    }

    private void handleSignInResult(GoogleSignInResult result) {

        if (result.isSuccess()) {

            GoogleSignInAccount acct = result.getSignInAccount();
            String name = acct.getDisplayName();
            String email = acct.getEmail();
            String photo = acct.getPhotoUrl().toString();
            updateUI(true, name, email, photo);
        } else {

            Toast.makeText(LoginActivity.this,AppConstants.KEY_ERROR,Toast.LENGTH_LONG);

        }
    }


    private void updateUI(boolean signedIn, String name, String email, String photo) {
        if (signedIn) {

            Intent i = new Intent(this, MainActivity.class);
            i.putExtra(AppConstants.KEY_NAME, name);
            i.putExtra(AppConstants.KEY_EMAIL, email);
            i.putExtra(AppConstants.KEY_PHOTO, photo);
            startActivity(i);
            finish();

        }
    }

    @Override
    protected void initComponent() {
       // FacebookDataTextView = (TextView) findViewById(R.id.TextView1);
        FBButton = (Button) findViewById(R.id.fbBtn);
        signInButton = (SignInButton) findViewById(R.id.btn_sign_in);

    }

    @Override
    protected void prepareViews() {

    }

    @Override
    protected void setListeners() {
        FBButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginManager.logInWithReadPermissions(LoginActivity.this, Arrays.asList("public_profile", "user_birthday", "email"));

            }
        });

        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                startActivityForResult(signInIntent, RC_SIGN_IN);
            }

        });
    }
}