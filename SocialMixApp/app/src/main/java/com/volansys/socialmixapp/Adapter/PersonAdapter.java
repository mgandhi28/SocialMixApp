package com.volansys.socialmixapp.Adapter;

import android.app.Activity;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.volansys.socialmixapp.Model.Person;
import com.volansys.socialmixapp.R;

import java.util.ArrayList;

/**
 * Created by Volansys on 3/1/18.
 */
public class PersonAdapter extends RecyclerView.Adapter<PersonAdapter.ViewHolder> {

    private ArrayList<Person> personArrayList = new ArrayList<>();
    private static final int LIST_ITEM = 0;
    private static final int GRID_ITEM = 1;
    boolean isSwitchView = true;
    Activity activity;


    public void removeAt(int position) {
        personArrayList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, personArrayList.size());
    }

    public PersonAdapter(ArrayList<Person> personArrayList) {
        this.personArrayList = personArrayList;

    }
    @Override
    public int getItemViewType (int position) {
        if (isSwitchView){
            return LIST_ITEM;
        }else{
            return GRID_ITEM;
        }
    }

    public boolean toggleItemViewType () {
        isSwitchView = !isSwitchView;
        return isSwitchView;
    }

    @Override
    public PersonAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Person person = personArrayList.get(position);
        holder.id.setText(person.getId());
        holder.name.setText(person.getName());
        holder.mobile.setText(person.getMobile());
        holder.city.setText(person.getCity());
if(person.getImage().equals(""))
{
    Picasso.with(activity)
            .load((Uri) null)
            .resize(300, 300)
            .into(holder.img);
}
else
    Picasso.with(activity)
            .load(person.getImage())
            .resize(300, 300)
            .into(holder.img);



        holder.date.setText(person.getDate());
        holder.company.setText(person.getCompany());
        holder.profile.setText(person.getProfessional());
        holder.gender.setText(person.getGender());
    }

    @Override
    public int getItemCount() {
        return personArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder  {
        public TextView id, name, mobile,city,date,company,profile,gender;
        public ImageView img;

        public ViewHolder(View view) {
            super(view);
            id = (TextView) view.findViewById(R.id.idTv);
            name = (TextView) view.findViewById(R.id.name);
            mobile = (TextView) view.findViewById(R.id.mobile);
            city = (TextView) view.findViewById(R.id.city);
            img = (ImageView) view.findViewById(R.id.img);
            date = (TextView) view.findViewById(R.id.date);
            company = (TextView) view.findViewById(R.id.company);
            profile = (TextView) view.findViewById(R.id.profile);
            gender = (TextView) view.findViewById(R.id.gender);

        }


    }

}

