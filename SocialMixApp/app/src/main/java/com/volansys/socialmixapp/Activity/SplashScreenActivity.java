package com.volansys.socialmixapp.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.volansys.socialmixapp.R;

/**
 * Created by navneet on 12/11/16.
 */

public class SplashScreenActivity extends BaseActivity {
    ImageView splashImage;
    Animation rotate, antiRotate, fade;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splashscreenmaker);


        initComponent();
        prepareViews();
        setListeners();

    }


    @Override
    protected void initComponent() {
          splashImage = (ImageView) findViewById(R.id.imageView);
          rotate = AnimationUtils.loadAnimation(getBaseContext(), R.anim.rotate);
          antiRotate = AnimationUtils.loadAnimation(getBaseContext(), R.anim.antirotate);
          fade = AnimationUtils.loadAnimation(getBaseContext(), R.anim.abc_fade_out);
    }

    @Override
    protected void prepareViews() {
        splashImage.startAnimation(antiRotate);
        antiRotate.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                splashImage.startAnimation(rotate);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        rotate.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                splashImage.startAnimation(fade);
                finish();
                Intent i = new Intent(getBaseContext(), LoginActivity.class);
                startActivity(i);
                finish();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }


    @Override
    protected void setListeners() {

    }
}
